<?php
    require_once(dirname(__FILE__)."/../log.php");
	writeLog("requests.txt");    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Landing page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
    .img-responsive {
 display: block;
 height: auto;
 max-width: 100%;
}</style>
  </head>
  <body>
    <div class="container">
      <div class="page-header" id="banner">        
        <div style="float: right">
            <a href="/lang/nl.php"><img src="/img/nl.gif" /></a>
            <a href="/lang/en.php"><img src="/img/en.gif" /></a>
        </div>
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">            
            <h1><img style="width: 50px; height: 50px; margin-bottom: 10px;" src="/img/nowifi.png" alt="NoWiFi" />You are <u>not</u> connected to the internet</h1>
            <p class="lead text-warning">Are you aware of the risks of public hotspots?</p>
          </div>
        </div>
      </div>
      
      <!-- Row 1 -->
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">          
          <p >Hi there! You just connected to this WiFi hotspot. Public WiFi hotspots can be useful for using the internet on your mobile device when you're away from home, but serious risks are attached to using them. Do you realise this?</p>
          <p>By connecting to this hotspot, your device becomes vulnerable. For example, data like usernames and password can be intercepted and abused by malicious users; even without you noticing it. 
        </div>
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="">          
         <img src="/img/threat.jpg" alt="Your network traffic might be intercepted." class="img-responsive" />
        </div>       
      </div>
      
      <!-- Row 2 -->     
      <div class="row"> 
        <div class="col-lg-6 col-md-6 col-sm-12" style="margin-top: 20px;">          
         <img src="/img/pass.jpg" alt="Passwords can be intercepted." class="img-responsive" />
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12">          
          <h2>Risks</h2>
          <p >Your Facebook, email or banking credentials might be visible for criminals, even if you don't notice anything. Also, they can intercept data about your identity and what websites you are visiting - not really a pleasant thought. Criminals can use this data to steal your money or identity, send you a lot of spam and other nasty things. Therefore it is important to be aware of the risks associated with public Wifi networks.</p>
          <p>This actually happens more often than you might think. Especially in big cities or crowded places fake hotspots may be present. Every year tens of people in The Netherlands become victims of fraud because they used malicious Wifi networks.</p>
        </div>
      </div>
      
      <!-- Row 3 -->     
      <div class="row"> 
        <div class="col-lg-6 col-md-6 col-sm-12">          
          <h2>Do's and Don'ts</h2>
            <p>- Try to use public Wifi hotspots <strong>as least as possible</strong>.</p>

            <p>- If Wifi is offered at a public place like a restaurant or a pub, <strong>first check the name</strong> of the network before connecting.</p>

            <p>- Be aware of the fact that when you are connected to the hotspot, everything you do on the internet <strong>can be seen</strong> by others. Adjust your behaviour.</p>

            <p>- Try to use <strong>web addresses that begin with 'https'</strong> as much as possible when connected to a hotspot. This makes it more difficult for potential eavesdroppers to intercept your data.</p>

            <p>- When you are connected to a secure network, <strong>change any password</strong> you used while connected to a public hotspot.</p>          
        </div>       
      </div>
 
      <!-- Notification -->
      <div class="alert alert-dismissable alert-warning">
        <h4>This hotspot does not provide an internet connection</h4>
        <p>Hopefully, this page made you aware of the dangers of public Wifi networks. This hotspot does not provide an internet connection, and of course does not intercept any data either.</p>
      </div>

      <!-- Copyright -->   
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" style="font-style: italic; "> 
          <p class="text-muted" style="text-align: center;">
            © 2014. All rights reserved.
          </p>
        </div>
      </div>           
    </div>
    
    <script>
        setTimeout(function(){
            var req = new XMLHttpRequest();
            req.open("get","/visited.php","true");
            req.send();
        },2000);
    </script>
  </body>
</html>
