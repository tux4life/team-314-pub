<?php
    require_once(dirname(__FILE__)."/../log.php");
    writeLog("requests.txt");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Landing page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
    .img-responsive {
 display: block;
 height: auto;
 max-width: 100%;
}</style>
  </head>
  <body>
    <div class="container">
      <div class="page-header" id="banner">        
        <div style="float: right">
            <a href="/lang/nl.php"><img src="/img/nl.gif" /></a>
            <a href="/lang/en.php"><img src="/img/en.gif" /></a>
        </div>
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">            
            <h1><img style="width: 50px; height: 50px; margin-bottom: 10px;" src="/img/nowifi.png" alt="NoWiFi" />Je bent <u>niet</u> verbonden met internet</h1>
            <p class="lead text-warning">Ben je je bewust van de gevaren van onbekende hotspots?</p>
          </div>
        </div>
      </div>
      
      <!-- Row 1 -->
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">          
          <p >Hallo! Je hebt zojuist verbinding gelegd met deze WiFi-hotspot. Openbare WiFi-hotspots kunnen handig zijn als je internet op je apparaat wil gebruiken als je van huis bent, maar er zitten serieuze gevaren aan het gebruik hiervan. Sta je daar wel eens bij stil?</p>
          <p>Door met deze hotspot te verbinden, maak je je apparaat kwetsbaar. Zo kunnen gegevens als inlognamen en wachtwoorden door kwaadwilligen worden afgeluisterd en worden misbruikt; zonder dat je dit in de gaten hebt. 
        </div>
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" style="">          
         <img src="/img/threat.jpg" alt="Je internetverkeer kan afgeluisterd worden." class="img-responsive" />
        </div>       
      </div>
      
      <!-- Row 2 -->     
      <div class="row"> 
        <div class="col-lg-6 col-md-6 col-sm-12" style="margin-top: 20px;">          
         <img src="/img/pass.jpg" alt="Wachtwoorden kunnen achterhaald worden." class="img-responsive" />
        </div> 
        <div class="col-lg-6 col-md-6 col-sm-12">          
          <h2>Gevaren</h2>
          <p >Zo kunnen je inloggegevens van Facebook, je e-mail, je bank ongemerkt in handen vallen van criminelen. Daarnaast kunnen ze onderscheppen wie je bent en welke website's je bezoekt - niet echt een prettig idee. Aan de hand van deze gegevens zouden criminelen je geld afhandig kunnen maken, je identiteit stelen, je grote hoeveelheden spam toesturen, en ga zo maar door. Daarom is het belangrijk dat je je bewust bent van de gevaren van openbare WiFi-netwerken. </p>
          <p>Dit gebeurt vaker dan je denkt. Met name in grote steden of op drukke plaatsen is de kans groter dat er valse WiFi-netwerken actief zijn. Jaarlijks zijn tientallen Nederlanders de dupe van fraude door valse WiFi-netwerken.</p>
        </div>
      </div>
      
      <!-- Row 3 -->     
      <div class="row"> 
        <div class="col-lg-6 col-md-6 col-sm-12">          
          <h2>Do's and Don'ts</h2>
            <p>- Probeer zo <strong>min mogelijk</strong> gebruik te maken van openbare WiFi-netwerken.</p>

            <p>- Wanneer een openbare gelegenheid gratis Wi-Fi heeft, zoals een restaurant of café, <strong>controleer dan eerst de naam</strong> van het netwerk voordat je hier verbinding mee maakt.</p>

            <p>- Ga er vanuit dat alles wat je doet terwijl je op de hotspot bent, <strong>gezien kan worden</strong> door anderen. Pas je gedrag daar op aan.</p>

            <p>- Probeer zoveel mogelijk gebruik te maken van <strong>webadressen die met 'https'</strong> beginnen als je met een hotspot verbonden bent. Hierdoor is het lastiger voor mogelijke afluisteraars om je gegevens te zien.</p>

            <p>- <strong>Verander wachtwoorden</strong> die je hebt gebruikt tijdens het gebruiken van een openbaar Wi-Fi netwerk zodra je weer op een beveiligd netwerk bent.</p>          
        </div>       
      </div>
 
      <!-- Notification -->
      <div class="alert alert-dismissable alert-warning">
        <h4>Deze hotspot biedt geen internet aan</h4>
        <p>Hopelijk ben je je bewust van de gevaren van openbare WiFi-netwerken dankzij deze informatie. Deze hotspot biedt zelf geen internet aan en onderschept uiteraard ook geen gegevens.</p>
      </div>

      <!-- Copyright -->   
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12" style="font-style: italic; "> 
          <p class="text-muted" style="text-align: center;">
            © 2014. Alle rechten voorbehouden.
          </p>
        </div>
      </div>           
    </div>
    
    <script>
        setTimeout(function(){
            var req = new XMLHttpRequest();
            req.open("get","/visited.php","true");
            req.send();
        },2000);
    </script>
  </body>
</html>
