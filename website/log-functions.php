<?php
    // Log requests
    function writeLog($file) {
        $session_file = dirname(__FILE__) . '/logs/current-session.txt';
        if (!file_exists($session_file)) die("logs/current-session.txt does not exist");
        
        $session = file_get_contents($session_file);
        $session_dir = dirname(__FILE__) . "/logs/$session";
        if (!file_exists($session_dir)) die("/logs/$session does not exist");
        
        $time = date('H:i:s d-m-Y');
        $cookie = (isset($_COOKIE['warning'])? $_COOKIE['warning'] : '');
        $warning = (showWarning() ? '1':'0');
        $log_entry = "$time;{$_SERVER['REMOTE_ADDR']};{$_SERVER['HTTP_USER_AGENT']};{$_SERVER['HTTP_ACCEPT_LANGUAGE']};$cookie;$warning";
        file_put_contents("$session_dir/$file", "$log_entry\n",  FILE_APPEND);
    }
    
    function showWarning(){
		if (isset($_COOKIE['warning'])){
			return $_COOKIE['warning'] == '1';
		} else {
			$hash = sha1($_SERVER['REMOTE_ADDR']);
			$warning = ($hash[0] >= '0' && $hash[0] < '8');
			setcookie('warning', $warning ? 1 :0, time()+60*60*24*365);
			return $warning;
		}
	}
	
	if(!isset($no_exit) && !showWarning()){
		writeLog("requests.txt");
?>
No internet connection
<img src="/img/nowifi.png?no-access"/>
    <script>
        setTimeout(function(){
            var req = new XMLHttpRequest();
            req.open("get","/no-access.php","true");
            req.send();
        },2000);
    </script>
<?php
		exit;
	}
?>
