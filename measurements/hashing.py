#!/usr/bin/python3

import sys
import hashlib
import binascii
import os

if len(sys.argv) < 1:
    raise Exception("Usage: ./hashing.py <filename>")

file = open(sys.argv[1])
tempfile = open("/tmp/somefile", "w+")

for line in file:
    # Replace a mac with its hash
    splitted = line.split(" ")
    splitted[7] = hashlib.sha1(splitted[7].encode("utf-8")).digest()
    splitted[7] = binascii.hexlify(splitted[7]).decode("ascii")[-6:]
    
    # Write the line to a temporary file (omitting the device name)
    tempfile.write(" ".join(splitted[:8]))
    tempfile.write("\n")

#Replace to original file with the temporary
os.rename("/tmp/somefile",sys.argv[1])
