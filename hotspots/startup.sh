#!/bin/bash

# Usage: ./startup.sh interface_1 interface_2 session_name
# interface_1: e.g. wlan0
# interface_2: e.g. wlan1, wlan5, ...
# session_name: used for logging. For example: mmdd-location. E.g. 1117-Valleilijn
interface1="$1"
interface2="$2"
session="$3"

# Configure logging
mkdir -p "../website/logs/$session/"
echo -n "$session" > "../website/logs/current-session.txt"

echo "ssid=WiFi
interface=$interface1
driver=nl80211
hw_mode=g
channel=6

ignore_broadcast_ssid=0
" > hostapd-wlan0.conf

echo "ssid=Hotspot
interface=$interface2
driver=nl80211
hw_mode=g
channel=6

ignore_broadcast_ssid=0
" > hostapd-wlan1.conf

sudo ip addr add 192.168.12.1/24 broadcast 192.168.12.255 dev "$interface1"
sudo ip addr add 192.168.13.1/24 broadcast 192.168.13.255 dev "$interface2"

sudo ip route add 192.168.12.0/24 dev "$interface1" src 192.168.12.1
sudo ip route add 192.168.13.0/24 dev "$interface2" src 192.168.13.1

sudo dnsmasq -C dnsmasq-wlan0.conf
sudo dnsmasq -C dnsmasq-wlan1.conf

sudo hostapd hostapd-wlan0.conf &
sudo hostapd hostapd-wlan1.conf
